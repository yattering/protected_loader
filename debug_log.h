/*SEH based VM Engine by Yattering, 2016
 *e-mail: yattering (at) sigaint (d0t) org
 *jabber: yattering (at) xmpp (d0t) jp
 */
 
#pragma once
#include <windows.h>

#ifdef __DEBUG__

inline size_t __DEBUG_strlen(const char *cs) {
    size_t len = 0;
    while (cs[len]) ++len;
    return len;
}

#define __DEBUG_INIT() \
  AllocConsole(); \
  SetConsoleTitle("Debug Information"); \
  __DEBUG_hDebugOutput = CreateFile("CONOUT$", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);\
  __DEBUG_hDebugInput = CreateFile("CONIN$", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

#define __DEBUG_GLOBAL_VARIABLES() \
  BYTE __DEBUG_Buffer[0x400];\
  LPVOID __DEBUG_pBuffer = (LPVOID)&__DEBUG_Buffer[0];\
  HANDLE __DEBUG_hDebugInput, __DEBUG_hDebugOutput;

#define __DEBUG_PRINTF(...) {\
    DWORD dwTemp;\
    int i = wsprintf(__DEBUG_pBuffer, __VA_ARGS__);\
    if ((i>0) && (i<0x400)) WriteFile(__DEBUG_hDebugOutput, __DEBUG_pBuffer, i, &dwTemp, NULL);\
}

#define __DEBUG_PUTS(message) {\
    DWORD dwTemp;\
    WriteFile(__DEBUG_hDebugOutput, (LPVOID)message, __DEBUG_strlen(message), &dwTemp, NULL);\
}

#define __DEBUG_GETS(buf) {\
    DWORD dwTemp;\
    ReadFile(__DEBUG_hDebugInput, (LPVOID)buf, 0x3FF, &dwTemp, NULL);\
}

#define __DEBUG_PAUSE() {\
    DWORD dwTemp;\
    ReadFile(__DEBUG_hDebugInput, (LPVOID)__DEBUG_pBuffer, 0x3FF, &dwTemp, NULL);\
}

#else
#define __DEBUG_GLOBAL_VARIABLES()
#define __DEBUG_INIT()
#define __DEBUG_PRINTF(message, ...)
#define __DEBUG_PUTS(message)
#define __DEBUG_GETS(buf)
#define __DEBUG_PAUSE()
#endif
